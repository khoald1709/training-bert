import random
import sys
import csv
import json
import pymysql.cursors
import numpy as np


with open('config_db.json', 'r') as f:
    config = json.load(f)


# Prepare data for training at the first time
def prepare_first_train():
    tsv_data_name = "../Tensorflow/hal_bh_4viewpoint-limit.tsv"

    random.seed(100)
    with open(tsv_data_name, 'r') as f, open("./data/rand-all.tsv", "w") as wf:
        lines = f.readlines()
        random.shuffle(lines)
        for line in lines:
            wf.write(line)

    random.seed(101)

    train_fname, dev_fname, test_fname = ["./data/train.tsv", "./data/dev.tsv", "./data/test.tsv"]

    with open("./data/rand-all.tsv") as f, open(train_fname, "w") as tf, open(dev_fname, "w") as df, open(test_fname,
                                                                                                          "w") as ef:
        ef.write("class\tsentence\n")
        for line in f:
            v = random.randint(0, 9)
            if v == 8:
                df.write(line)
            elif v == 9:
                ef.write(line)
            else:
                tf.write(line)


def check():
    with open("./data/test.tsv") as f, open("./data/output/hal_output_predic/test_results.tsv") as rf:
        test = csv.reader(f, delimiter='\t')
        test_result = csv.reader(rf, delimiter='\t')

        # 正解データの抽出
        next(test)
        test_list = [int(row[1]) for row in test]

        # 予測結果を抽出
        result_list = [0 if row[0] > row[1] else 1 for row in test_result]

        test_count = len(test_list)
        result_correct_answer_list = [result for test, result in zip(test_list, result_list) if test == result]
        result_correct_answer_count = len(result_correct_answer_list)
        print("Tỷ lệ trả lời đúng: ", result_correct_answer_count / test_count)


# texture
def prepare_texture():
    view_point = "./data/input/texture"
    tsv_data_name = "/viewpoint.tsv"

    random.seed(0)
    with open(view_point + tsv_data_name, 'r') as f, open(view_point + "/rand-all.tsv", "w") as wf:
        lines = f.readlines()
        random.shuffle(lines)
        for line in lines:
            wf.write(line)

    random.seed(101)

    train_fname, dev_fname, test_fname = [view_point + "/train.tsv", view_point + "/dev.tsv", view_point + "/test.tsv"]

    with open(view_point + "/rand-all.tsv") as f, open(train_fname, "w") as tf, open(dev_fname, "w") as df, open(
            test_fname, "w") as ef:
        ef.write("class\tsentence\n")
        for line in f:
            v = random.randint(0, 9)
            if v == 8:
                df.write(line)
            elif v == 9:
                ef.write(line)
            else:
                tf.write(line)


def check_texture():
    view_point = "./data/input/texture"
    rate = "0.95"

    with open(view_point + "/test.tsv") as f, open("./data/tmp/texture_output_predic/test_results.tsv") as rf:
        test = csv.reader(f, delimiter='\t')
        test_result = csv.reader(rf, delimiter='\t')

        # Trích xuất dữ liệu của Ground truth
        next(test)
        test_list = [int(row[1]) for row in test]

        # Trích xuất kết quả dự đoán
        # Do là 2 giá trị cho nên sẽ chọn cái nào mà có score lớn hơn
        # result_list = [0 if row[0] > rate else 1 for row in test_result ]
        # 予測結果を抽出
        result_list = []
        for result in test_result:
            print(result)
            exit()
            max_index = np.argmax(result)
            result_list.append(max_index)

        # 分類した予測結果(カテゴリNo)を出力
        with open('./data/tmp/texture_output_predic/test_results.csv', 'w') as of:
            writer = csv.writer(of)
            for row in result_list:
                writer.writerow([row])

        test_count = len(test_list)
        result_correct_answer_list = [result for test, result in zip(test_list, result_list) if test == result]
        result_correct_answer_count = len(result_correct_answer_list)
        print("Tỷ lệ trả lời đúng: ", result_correct_answer_count / test_count)

        print("==========================================")

        test_count = len(test_list)
        result_correct_answer_list = [result for test, result in zip(test_list, result_list) if test == result]
        result_correct_answer_count = len(result_correct_answer_list)
        print("Số lượng dữ liệu thử nghiệm: ", test_count)
        test_positive_list = [test for test in test_list if test == 0]
        test_positive_count = len(test_positive_list)
        print("Số lượng dữ liệu áp dụng trong dữ liệu thử nghiệm: ", test_positive_count)
        print("Tỷ lệ trả lời đúng: ", result_correct_answer_count / test_count)

        result_positive_list = [result for result in result_list if result == 0]
        result_positive_count = len(result_positive_list)
        result_false_positive_list = [result for test, result in zip(test_list, result_list) if
                                      test == 1 and result == 0]
        result_false_positive_count = len(result_false_positive_list)
        print("Số lượng tích cực: ", result_positive_count)
        print("Tỉ lệ tích cực: ",
              result_false_positive_count / result_positive_count if result_positive_count > 0 else 0)

        result_negative_list = [result for result in result_list if result == 1]
        result_negative_count = len(result_negative_list)
        result_false_negative_list = [result for test, result in zip(test_list, result_list) if
                                      test == 0 and result == 1]
        result_false_negative_count = len(result_false_negative_list)
        print("Số lượng tiêu cực: ", result_negative_count)
        print("Tỉ lệ tiêu cực: ",
              result_false_negative_count / result_negative_count if result_negative_count > 0 else 0)


def sentences_export():
    try:
        query = "SELECT id, replace(replace(replace(body, '\r\n', ''), '\r', ''), '\n', '') as body FROM " \
                "review_sentences LIMIT 200 "

        print("==== Connect database ====")
        analyzer_connection = pymysql.connect(host=config['analyzer_db_host'],
                                              user=config['analyzer_db_user'],
                                              password=config['analyzer_db_pass'],
                                              db=config['analyzer_db_name'])

        cursor = analyzer_connection.cursor()
        cursor.execute(query)

        with open(f"./data/input/review_sentences.tsv", "w+", newline='') as outfile:
            writer = csv.writer(outfile, delimiter="\t", quoting=csv.QUOTE_ALL)
            for row in cursor.fetchall():
                writer.writerow(row)

            outfile.close()

        print("==== Disconnect database ====")
        analyzer_connection.close()
    except Exception as e:
        print("Export CSV Fail")
        print(e)


def prepare_sentences():
    tsv_data_name = "./data/input/review_sentences.tsv"

    random.seed(100)
    with open(tsv_data_name, 'r') as f, open("./data/input/sentences/rand-all.tsv", "w") as wf:
        lines = f.readlines()
        random.shuffle(lines)
        for line in lines:
            wf.write(line)

    random.seed(101)

    train_fname, dev_fname, test_fname = ["./data/input/sentences/train.csv", "./data/input/sentences/dev.csv",
                                          "./data/input/sentences/test.csv"]

    with open("./data/input/sentences/rand-all.tsv") as f, open(train_fname, "w") as tf, open(dev_fname,
                                                                                              "w") as df, open(
        test_fname, "w") as ef:
        # ef.write("class\tsentence\n")
        for line in f:
            line = line.replace("\t", ",")
            v = random.randint(0, 9)
            if v == 8:
                df.write(line)
            elif v == 9:
                ef.write(line)
            else:
                tf.write(line)


def test():
    import tokenization
    from transformers import BertModel, BertConfig
    import torch

    tokenizer = tokenization.FullTokenizer("./Japanese_L-12_H-768_A-12_E-30_BPE/vocab.txt", do_lower_case=False)

    text = "パッケージがかわいいです。オイルですがさらっとして水っぽい使い心地です。W洗顔必要ないのも楽ちんです。コスパが良い！"

    print(tokenizer.convert_ids_to_tokens(tokenizer.convert_tokens_to_ids(tokenizer.tokenize(text))))
    tokenized_text = tokenizer.tokenize(text)
    tokenized_text.insert(0, '[CLS]')
    tokenized_text.append('[SEP]')
    masked_index = 1
    tokenized_text[masked_index] = '[MASK]'

    print(tokenized_text)

    config = BertConfig.from_json_file('./Japanese_L-12_H-768_A-12_E-30_BPE/bert_config.json')
    model = BertModel.from_pretrained('./Japanese_L-12_H-768_A-12_E-30_BPE', config)

    tokens = tokenizer.convert_tokens_to_ids(tokenized_text)
    tokens_tensor = torch.tensor([tokens])
    model.eval()

    with torch.no_grad():
        outputs = model(tokens_tensor)
        predictions = outputs[0]

    _, predict_indexes = torch.topk(predictions[0, masked_index], k=5)
    predict_tokens = tokenizer.convert_ids_to_tokens(predict_indexes.tolist())
    print(predict_tokens)


if __name__ == '__main__':
    globals()[sys.argv[1]]()
