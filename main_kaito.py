import os
import csv
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

data_path = "test_multi_label.csv"

data_raw = pd.read_csv(data_path)

print("Number of rows in data =", data_raw.shape[0])
print("Number of columns in data =", data_raw.shape[1])
print("\n")
print("**Sample data:**")

data_raw.head()

# categories = list(data_raw.columns[2:].values)
# print(data_raw.iloc[:, 2:].sum().values)
# print(categories)
# sns.set(font_scale=1)
# plt.figure(figsize=(15, 8))
# ax = sns.barplot(categories, data_raw.iloc[:, 2:].sum().values)
# plt.title("Comments in each category", fontsize=24)
# plt.ylabel('Number of comments', fontsize=18)
# plt.xlabel('Comment Type ', fontsize=18)
#
# #
# rects = ax.patches
# labels = data_raw.iloc[:, 2:].sum().values
# print(labels)
# for rect, label in zip(rects, labels):
#     height = rect.get_height()
#     ax.text(rect.get_x() + rect.get_width() / 2, height + 5, label, ha='center', va='bottom', fontsize=18)
# plt.show()

# Counting the number of comments having multiple labels.

# rowSums = data_raw.iloc[:,2:].sum(axis=1)
# multiLabel_counts = rowSums.value_counts()
# multiLabel_counts = multiLabel_counts.iloc[1:]
# print(multiLabel_counts)
#
# sns.set(font_scale = 1)
# plt.figure(figsize=(15,8))
#
# ax = sns.barplot(multiLabel_counts.index, multiLabel_counts.values)
# plt.title("Comments having multiple labels ")
# plt.ylabel('Number of comments', fontsize=18)
# plt.xlabel('Number of labels', fontsize=18)
#
# rects = ax.patches
# labels = multiLabel_counts.values
# for rect, label in zip(rects, labels):
#     height = rect.get_height()
#     ax.text(rect.get_x() + rect.get_width()/2, height + 5, label, ha='center', va='bottom')
#
# plt.show()

from wordcloud import WordCloud, STOPWORDS

plt.figure(figsize=(40, 25))
# clean
subset = data_raw[data_raw.cost_performance == True]
text = subset.body.values
cloud_toxic = WordCloud(
    stopwords=STOPWORDS,
    background_color='black',
    collocations=False,
    width=2500,
    height=1800
).generate(" ".join(text))
plt.axis('off')
plt.title("Cost Performance", fontsize=40)
plt.imshow(cloud_toxic)
