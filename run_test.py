import csv


# Test result from multi-labels trained model

def _check() :
    data_dir = "./data/output"
    with open("test.tsv") as f, open(data_dir + "test_results.tsv") as rf:
        test = csv.reader(f, delimiter='\t')
        test_result = csv.reader(rf, delimiter='\t')
        next(test)
